from builtins import staticmethod

from rest_framework import views
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from django.contrib.auth.models import User


def token_response(token, user):
    return {
        "token": token.key,
        "user_id": user.pk,
        "email": user.email,
        "username": user.username,
    }


class VerifyTokenView(views.APIView):
    @staticmethod
    def post(request):
        token = request.META['HTTP_AUTHORIZATION'].split()[1]
        token = Token.objects.get(key=token)
        user = User.objects.get(username=token.user)
        return Response(status=200, data=token_response(token, user))

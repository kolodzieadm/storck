from django.urls import path

from .views import VerifyTokenView

urlpatterns = [
    path('auth', VerifyTokenView.as_view(), name='api_token_verify'),
]

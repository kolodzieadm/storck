import hashlib
import os
from django.conf import settings
from multiprocessing import Process, Queue


def get_storck_file_path(file_object):
    return os.path.join(settings.MEDIA_ROOT, str(file_object.file))


def hash_data(data):
    hasher = hashlib.md5()
    hasher.update(data)
    return hasher.hexdigest()


def hash_file(fileUrl):
    with open(fileUrl, 'rb') as file:
        data = file.read()
    return hash_data(data)

def hash_chunked_file(f):
    file_hash = hashlib.md5()
    while chunk := f.read(8192):
        file_hash.update(chunk)

    return file_hash.hexdigest()


def hash_chunked_path(file_path):
    with open(file_path, "rb") as f:
        file_hash = hash_chunked_file(f)
    return file_hash

def hash_large_multi(storck_file):
    if isinstance(storck_file, str):
        hasher = hash_chunked_path
    else:
        hasher = hash_chunked_file
    def fun(_file, q):
        file_hash = hasher(_file)
        q.put(file_hash)
    q = Queue()
    p = Process(target=fun, args=(storck_file,q))
    p.start()
    _hash = q.get()
    p.join()
    return _hash

from pathlib import Path
import os
from django.http import HttpResponse
from django.db.models import Q
from django.core.files import File as DjangoFile
from django.conf import settings
from django.contrib.auth.models import User
from rest_framework import views
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import permission_classes
from .utils import hash_file, get_storck_file_path, hash_large_multi, hash_chunked_file
from .models import StorckFile
from .forms import FileForm
from workspace.models import WorkspaceToken, Workspace


class ListFileView(views.APIView):
    def get(self, request):
        workspace_token = request.GET.get("token", None)
        if workspace_token is None:
            return Response(status=403, data={"message": "Missing workspace token"})
        workspace = WorkspaceToken.objects.get(token=workspace_token).workspace
        hidden_files = request.GET.get("hidden", False)
        queryset = None
        if hidden_files == "True":
            queryset = StorckFile.objects.filter(workspace=workspace).values()
        else:
            queryset = StorckFile.objects.filter(workspace=workspace, hide=False).values()
        return Response({"files": list(queryset)})

class LocalFileError(Exception):
    pass

class FileView(views.APIView):
    parser_classes = (
        FormParser,
        MultiPartParser,
    )

    def prepare_response(self, request, data_qs):
        if data_qs is None:
            return Response(status=404, data={"message": "StorckFile not found"})
        if "info" in request.query_params and request.query_params["info"] == "True":
            return Response({"file": data_qs.values().first()})
        else:
            filemodel_object = data_qs.first()
            if filemodel_object.duplicate_of is None:
                fp = filemodel_object.file
            else:
                fp = filemodel_object.duplicate_of.file
            data = fp.open().read()
            fp.close()
            response = HttpResponse(data, content_type="application/force-download")
            filename = Path(filemodel_object.fake_path).name
            response['Content-Disposition'] = 'attachment; filename="{}"'.format(filename)
            return response

    def get_by_path(self, request, workspace):
        fake_path = request.GET.get("path")
        if fake_path is None:
            return Response(status=400, data={"message": "missing id or path"})
        else:
            _files = StorckFile.objects.filter(fake_path=fake_path, workspace=workspace)
            if not _files:
                return Response(status=404, data={"message": "File not found"})
            else:
                data_qs = _files.order_by("-id")
        return self.prepare_response(request, data_qs)

    def get(self, request):
        workspace_token = request.GET.get("token", None)
        if workspace_token is None:
            return Response(status=403, data={"message": "Missing workspace token"})
        workspace = WorkspaceToken.objects.get(token=workspace_token).workspace
        file_id = request.GET.get("id")
        if file_id is None:
            response = self.get_by_path(request, workspace)
        else:
            data_qs = StorckFile.objects.filter(id=file_id, workspace=workspace)
            response = self.prepare_response(request, data_qs)
        return response

    # def depr_add_by_local(self, request):
    #     # https://stackoverflow.com/questions/3501588/how-to-assign-a-local-file-to-the-filefield-in-django
    #     # Ideally the file should not be read but only assigned, if this is not the case in new django
    #     # the workaraound is in the link above
    #     local_path = request.POST['local_path']
    #     local_file_path = Path(local_path)
    #     if local_file_path.is_file():
    #         local_hash=hash_large_multi(local_path)
    #         if settings.REQUIRE_HASH:
    #             file_hash = request.POST['hash']
    #             if file_hash!=local_hash:
    #                 #when hash does not match u0p
    #                 return Response()

    #         existing = Path(settings.MEDIA_ROOT)/file_hash
    #         if existing.is_file():
    #             pass
    #         else:
    #             local_file_path.rename(existing)
    #     form = FileForm(request.POST)
    #     if form.is_valid():
    #         storck_file = form.save(commit=False)
    #         new
    #     pass

    # def add_by_request(self, request):
    #     workspace_token = request.GET["token"]
    #     token = request.headers.get("Authorization").replace("Token ", "")
    #     workspace = WorkspaceToken.objects.get(token=workspace_token).workspace
    #     token_user = Token.objects.get(key=token).user
    #     form = FileForm(request.POST, request.FILES)
    #     if form.is_valid():
    #         new_file = self.successful_update(
    #             request=request,
    #             new_file=form.save(commit=False),
    #             token_user=token_user,
    #             workspace=workspace,
    #         )
    #         previd = (
    #             new_file.previous_version.id
    #             if new_file.previous_version is not None
    #             else None
    #         )
    #         return Response(
    #             status=200,
    #             data={
    #                 "id": new_file.id,
    #                 "duplicate": new_file.duplicate_of is not None,
    #                 "previous_version": previd,
    #             },
    #         )
    #     return Response(status=400)

    # def npost(self, request):
    #     workspace_token = request.GET.get("token", None)
    #     if workspace_token is None:
    #         return Response(status=403, data={"message": "Missing workspace token"})
    #     if request.POST.get('local', False):
    #         response = self.add_by_local(request)
    #     else:
    #         response = self.add_by_request(request)
    #     return response

    def check_duplicate(self, file_hash):
        duplicate = StorckFile.objects.filter(hash=file_hash)
        #assert duplicate.count() <= 1
        if duplicate.exists():
            duplicated_file = duplicate.order_by('id')[0]
        else:
            duplicated_file = None
        return duplicated_file

    def check_previous_version(self, fake_path, workspace):
        previous_versions = StorckFile.objects.filter(
            fake_path=fake_path, workspace=workspace
        )
        # @TODO check if it excludes current
        # previous_versions = previous_versions.exclude(id=object_id)
        last_version = previous_versions.order_by("-id").first()
        if last_version is not None:
            last_version.hide = True
            last_version.save()
        return last_version

    def process_duplicate(self, duplicated_record, uploaded_file):
        if duplicated_record is None:
            # @TODO copy a file into right place
            rfile = uploaded_file
        else:
            rfile = None
        return rfile

    def process_transaction(self, request, file_hash, uploaded_file, fake_path, workspace):
        duplicated_record = self.check_duplicate(file_hash)
        duplicated_file = self.process_duplicate(duplicated_record, uploaded_file)
        previous_version = self.check_previous_version(fake_path, workspace)
        meta_open = request.POST.get("meta", None)
        duplicate_of = duplicated_file
        return duplicated_record, duplicated_file, previous_version, meta_open

    def process_local_file(self, request, local_hash, local_path):
        # https://stackoverflow.com/questions/3501588/how-to-assign-a-local-file-to-the-filefield-in-django
        # Ideally the file should not be read but only assigned, if this is not the case in new django
        # the workaraound is in the link above
        local_file_path = Path(local_path)
        if local_file_path.is_file():
            if settings.REQUIRE_HASH:
                file_hash = request.POST['hash']
                if file_hash!=local_hash:
                    raise LocalFileError("given hash is not matching sent hash")
            existing = Path(settings.MEDIA_ROOT)/file_hash
            if existing.is_file():
                local_file_path.unlink()
                return file_hash
            else:
                # TODO should check for dots '/../'
                local_file_path.rename(existing)
        else:
            raise LocalFileError("local file not found")
        return file_hash


    def process_file(self, request):
        local_path = request.POST.get('local_path')
        request_file = request.FILES.get('file', None)
        if request_file is not None:
            f = request_file.open('rb')
            # file_hash = hash_large_multi(f)
            file_hash = hash_chunked_file(f)
            uploaded_file = DjangoFile(f, name=request_file.name)
        elif request.POST.get('local', False) and local_path:
            file_hash = self.process_local_file(request, local_path)
            uploaded_file = DjangoFile(open(file_hash), name=file_hash)
        else:
            raise LocalFileError('no file specified')
        return uploaded_file, file_hash


    @permission_classes([IsAuthenticated])
    def post(self, request):
        # form = FileForm(request.POST)
        # if form.is_valid():
        # new_file = form.save(commit=False)
        try:
            uploaded_file, file_hash = self.process_file(request)
        except LocalFileError as e:
            return Response(status=400, data={'message': str(e)})
        workspace_token = request.GET.get("token", None)
        if workspace_token is None:
            response = Response(status=403, data={"message": "Missing workspace token"})
            return response
        workspace = WorkspaceToken.objects.get(token=workspace_token).workspace
        workspace_is_users = Workspace.objects.filter(pk=workspace.pk, users__pk=request.user.pk).exists()
        fake_path = request.POST.get("path", os.path.basename(uploaded_file.name))
        if workspace_is_users:
            duplicated_record, duplicated_file, previous, meta = self.process_transaction(request, file_hash, uploaded_file, fake_path, workspace)
            new_file = StorckFile(
                user = request.user,
                file=duplicated_file,
                duplicate_of=duplicated_record,
                previous_version=previous,
                meta_open=meta,
                hash=file_hash,
                fake_path=fake_path,
                workspace=workspace,
            )
            new_file.save()

            return Response(
                status=200,
                data={
                    "id": new_file.id,
                    "duplicate": new_file.duplicate_of is not None,
                    "previous_version": previous.id if previous is not None else None,
                },
            )
        else:
            response = Response(status=403, data={"message": "workspace access denied"})
        return response





    def successful_update(self, request, new_file, token_user, workspace):
        # @TODO this later should moved into model
        # also how to handle file before saving it in the database :
        # https://docs.djangoproject.com/en/3.0/topics/http/file-uploads/
        new_file.user_id = token_user.pk
        new_file.workspace = workspace
        new_file.save(commit=False)

        fake_path = request.POST.get("path", os.path.basename(new_file.file.name))
        meta_open = request.POST.get("meta", None)

        file_path = get_storck_file_path(new_file)
        hash = hash_file(file_path)
        duplicate_of = StorckFile.objects.filter(hash=hash).first()

        if duplicate_of:
            self.handle_duplicates(
                request=request,
                new_file=new_file,
                duplicate_of=duplicate_of,
            )

            hash = duplicate_of.hash

        prev_version = self.handle_prev_versions(
            fake_path=fake_path,
            workspace=workspace,
            object_id=new_file.id,
        )

        new_file.hash = hash
        new_file.fake_path = fake_path
        new_file.meta_open = meta_open
        new_file.previous_version = prev_version
        new_file.save()
        return new_file

    def handle_duplicates(self, request, new_file, duplicate_of):
        new_file.file.delete()
        new_file.file = duplicate_of.file
        new_file.duplicate_of = duplicate_of

    def handle_prev_versions(self, fake_path, workspace, object_id):
        previous_versions = StorckFile.objects.filter(
            fake_path=fake_path, workspace=workspace
        )
        previous_versions = previous_versions.exclude(id=object_id)
        prev_version = previous_versions.order_by("-id").first()
        if prev_version is not None:
            prev_version.hide = True
            prev_version.save()
        return prev_version

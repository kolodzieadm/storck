from django.test import TestCase
from django.core.files import File
from django.contrib.auth.models import User
from workspace.models import Workspace, WorkspaceToken
from rest_framework.test import APITestCase, URLPatternsTestCase, APIClient
from rest_framework.authtoken.models import Token
from django.urls import reverse, path, include
import random
import string
import tempfile
import os
import tempfile

import datetime
from file.models import StorckFile
from file.views import LocalFileError, FileView
import file.views
from file.utils import hash_data, hash_file
from unittest.mock import MagicMock, patch, mock_open
from pathlib import Path


class GeneralFileTest:
    def setUp(self):
        self.email = "foo@bar.com"
        self.user_obj = User.objects.create_user(username="test", email=self.email)
        self.password = "some_password"
        user_token = "7c817d0d8f0e2b719a9df798fbdefe75cf5ba4be"
        self.token = Token.objects.create(user=self.user_obj, key=user_token)
        self.user_obj.set_password(self.password)
        self.user_obj.save()
        self.workspace_obj = Workspace.objects.create()
        self.workspace_obj.save()
        self.workspace_obj.users.add(self.user_obj)
        self.workspace_obj.save()
        workspace_token = "73f19f87-f741-4f53-ae50-a9272dc87ea7"
        self.wrksp_token = WorkspaceToken.objects.create(
            workspace=self.workspace_obj,
            name="Default token",
            token=workspace_token,
        )

        f = open("examples/exampleFile.txt")
        example_file = File(f)
        StorckFile.objects.create(
            file=example_file, user=self.user_obj, workspace=self.workspace_obj
        )
        self.file_obj = StorckFile.objects.first()
        self.file_obj2 = StorckFile.objects.create(
            file=example_file,
            user=self.user_obj,
            workspace=self.workspace_obj,
            fake_path="fake/path",
            meta_open="some data",
            meta_closed="some different data",
            hide=True,
        )
        self.generate_random_string = lambda x: "".join(
            random.choices(string.ascii_uppercase + string.digits, k=x)
        )


class FileModelTestCase(TestCase, GeneralFileTest):
    def __init__(self, *args, **kwargs):
        TestCase.__init__(self, *args, **kwargs)
        GeneralFileTest.__init__(self)

    def setUp(self):
        GeneralFileTest.setUp(self)

    def test_model_return_correct_file(self):
        file_path = StorckFile.objects.first().file
        self.assertEqual("example" in str(file_path), True)

    def test_attributes(self):
        f = StorckFile.objects.first()
        self.assertTrue(hasattr(f, "id"))
        self.assertTrue(hasattr(f, "file"))
        self.assertTrue(hasattr(f, "hash"))
        self.assertTrue(hasattr(f, "workspace"))
        self.assertTrue(hasattr(f, "user"))
        self.assertTrue(hasattr(f, "previous_version"))
        self.assertTrue(hasattr(f, "duplicate_of"))
        self.assertTrue(hasattr(f, "date"))
        self.assertTrue(hasattr(f, "fake_path"))
        self.assertTrue(hasattr(f, "meta_open"))
        self.assertTrue(hasattr(f, "meta_closed"))
        self.assertTrue(hasattr(f, "hide"))

    def test_hash_data(self):
        random_string1 = self.generate_random_string(100).encode("utf-8")
        random_string2 = self.generate_random_string(100).encode("utf-8")
        if random_string1 == random_string2:
            random_string2 = self.generate_random_string(100).encode("utf-8")
        hash1a = hash_data(random_string1)
        hash1b = hash_data(random_string1)
        hash2 = hash_data(random_string2)
        self.assertNotEqual(hash1a, hash2)
        self.assertEqual(hash1a, hash1b)

    def test_hash_file(self):
        random_string1 = self.generate_random_string(100)
        random_string2 = self.generate_random_string(100)
        if random_string1 == random_string2:
            random_string2 = self.generate_random_string(100)
        tmpfile1a = tempfile.NamedTemporaryFile("w", delete=False)
        tmpfile1a.write(random_string1)
        filepath1a = tmpfile1a.name
        tmpfile1b = tempfile.NamedTemporaryFile("w", delete=False)
        tmpfile1b.write(random_string1)
        filepath1b = tmpfile1b.name
        tmpfile2 = tempfile.NamedTemporaryFile("w", delete=False)
        tmpfile2.write(random_string2)
        filepath2 = tmpfile2.name
        tmpfile1a.close()
        tmpfile1b.close()
        tmpfile2.close()
        hash1a = hash_file(filepath1a)
        hash1b = hash_file(filepath1b)
        hash2 = hash_file(filepath2)
        self.assertEqual(hash1b, hash1a)
        self.assertNotEqual(hash2, hash1a)
        os.remove(tmpfile1a.name)
        os.remove(tmpfile1b.name)
        os.remove(tmpfile2.name)

    def test_hash(self):
        self.assertIsNone(self.file_obj.hash)

    def test_workspace(self):
        self.assertEqual(self.file_obj.workspace, self.workspace_obj)

    def test_user(self):
        self.assertEqual(self.file_obj.user, self.user_obj)

    def test_previous_version(self):
        # @TODO this is missing the actual test case now, as setting this
        # is moved into api
        self.assertIsNone(self.file_obj.previous_version)

    def test_duplicate_of(self):
        # @TODO this is missing the actual test case now, as setting this
        # is moved into api
        self.assertIsNone(self.file_obj.duplicate_of)

    def test_date(self):
        diff = self.file_obj.date - datetime.datetime.now(datetime.timezone.utc)
        self.assertGreater(60, diff.total_seconds())

    def test_fake_path(self):
        # @TODO this is missing the actual test case now, as setting this
        # is moved into api
        self.assertIsNone(self.file_obj.fake_path)
        self.assertEqual(self.file_obj2.fake_path, "fake/path")

    def test_meta_open(self):
        self.assertIsNone(self.file_obj.meta_open)
        self.assertEqual(self.file_obj2.meta_open, "some data")

    def test_meta_closed(self):
        self.assertIsNone(self.file_obj.meta_closed)
        self.assertEqual(self.file_obj2.meta_closed, "some different data")

    def test_hide(self):
        self.assertFalse(self.file_obj.hide)
        self.assertTrue(self.file_obj2.hide)


class FileViewTests(APITestCase, URLPatternsTestCase, GeneralFileTest):
    urlpatterns = [
        path("api/", include("file.urls")),
    ]

    def __init__(self, *args, **kwargs):
        APITestCase.__init__(self, *args, **kwargs)
        URLPatternsTestCase.__init__(self, *args, **kwargs)
        GeneralFileTest.__init__(self)

    def setUp(self):
        GeneralFileTest.setUp(self)

    def test_get_by_path_list(self):
        # client.login(email=self.email, password=self.password)
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        response = self.client.get(
            reverse("all-files"),
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.data, {"message": "Missing workspace token"})
        token = WorkspaceToken.objects.filter(workspace=self.workspace_obj).first()
        # breakpoint()
        response2 = self.client.get(reverse("all-files"), {"token": token.token})
        queryset = StorckFile.objects.filter(
            workspace=self.workspace_obj, hide=False
        ).values()
        self.assertDictEqual({"files": list(queryset)}, response2.data)

        response3 = self.client.get(
            reverse("all-files"), {"token": token.token, "hidden": True}
        )
        queryset = StorckFile.objects.filter(workspace=self.workspace_obj).values()
        self.assertDictEqual({"files": list(queryset)}, response3.data)

    def test_get_file(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        response = self.client.get(
            reverse("single-file"),
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.data, {"message": "Missing workspace token"})

        response = self.client.get(
            reverse("single-file"), {"token": self.wrksp_token.token}
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, {"message": "missing id or path"})

        response = self.client.get(
            reverse("single-file"),
            {"token": self.wrksp_token.token, "path": "df;lkj;lkjw;lkj"},
        )
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.data, {"message": "File not found"})

        response = self.client.get(
            reverse("single-file"),
            {"token": self.wrksp_token.token, "path": self.file_obj2.fake_path},
        )
        self.assertEqual(response.status_code, 200)
        fp = self.file_obj2.file
        data = fp.open().read()
        self.assertEqual(response.content, data)
        fp.close()

        response = self.client.get(
            reverse("single-file"),
            {"token": self.wrksp_token.token, "id": self.file_obj2.id},
        )
        self.assertEqual(response.status_code, 200)
        fp = self.file_obj2.file
        data = fp.open().read()
        self.assertEqual(response.content, data)
        fp.close()

    def test_post_file(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        response = self.client.post(
            reverse("single-file"),
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, {"message": "no file specified"})


    @patch('file.views.settings.REQUIRE_HASH', new=True)
    def test_process_local_file(self):
        tmpfile_path = 'tmp1.txt'
        request = MagicMock()
        request.POST = {'hash': "aaaa"}
        local_hash = "aaaa"
        with tempfile.TemporaryDirectory() as tmpdirname:
            with patch('file.views.settings.MEDIA_ROOT', new=tmpdirname):
                with self.assertRaisesRegex(LocalFileError,  "local file not found") as err:
                    result = FileView.process_local_file(None, request, local_hash, tmpfile_path)
                self.assertFalse((Path(tmpdirname)/local_hash).is_file())

                tmpfile_path = Path(tmpdirname)/'tmp1.txt'
                with open(tmpfile_path, 'w') as f1:
                    f1.write("test1")
                request = MagicMock()
                request.POST = {'hash': "aaaa"}
                local_hash = "bbbb"
                with patch('file.views.settings.REQUIRE_HASH', new=True):
                    with self.assertRaisesRegex(LocalFileError,"given hash is not matching sent hash") as err:
                        result = FileView.process_local_file(None, request, local_hash, tmpfile_path)
                self.assertFalse((Path(tmpdirname)/local_hash).is_file())

                local_hash = "aaaa"
                with patch('file.views.settings.REQUIRE_HASH', new=True):
                    result = FileView.process_local_file(None, request, local_hash, tmpfile_path)
                self.assertEquals(result, request.POST['hash'])
                self.assertTrue((Path(tmpdirname)/local_hash).is_file())
                self.assertFalse(tmpfile_path.exists())

                with open(tmpfile_path, 'w') as f1:
                    f1.write("test1")
                with patch('file.views.settings.REQUIRE_HASH', new=True):
                    result = FileView.process_local_file(None, request, local_hash, tmpfile_path)
                self.assertEquals(result, request.POST['hash'])
                self.assertFalse(tmpfile_path.exists())


    def test_post_without_file(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        response = self.client.post(
            reverse("single-file") + "?token={}".format(self.wrksp_token.token),
        )

        self.assertEqual(response.status_code, 400)

    def test_post_with_file(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        with open("examples/exampleFile3.txt", "rb") as output:
            response = self.client.post(
                reverse("single-file") + "?token={}".format(self.wrksp_token.token),
                {"path": "fake/path0", "file": output},
            )
        self.assertEqual(response.status_code, 200)
        self.assertFalse(response.data["duplicate"])
        self.assertIsNone(response.data["previous_version"])

    def test_post_with_duplicate(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        with open("examples/exampleFile3.txt", "rb") as output:
            response = self.client.post(
                reverse("single-file") + "?token={}".format(self.wrksp_token.token),
                {"path": "fake/path0", "file": output},
            )
        with open("examples/exampleFile3.txt", "rb") as output:
            response = self.client.post(
                reverse("single-file") + "?token={}".format(self.wrksp_token.token),
                {"path": "fake/path2", "file": output},
            )
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.data["duplicate"])
        self.assertIsNone(response.data["previous_version"])

    def test_post_with_previous(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        with open("examples/exampleFile3.txt", "rb") as output:
            response = self.client.post(
                reverse("single-file") + "?token={}".format(self.wrksp_token.token),
                {"path": "fake/path2", "file": output},
            )
        previd = response.data["id"]
        with open("examples/exampleFile2.txt", "rb") as output:
            response = self.client.post(
                reverse("single-file") + "?token={}".format(self.wrksp_token.token),
                {"path": "fake/path2", "file": output},
            )
        self.assertEqual(response.status_code, 200)
        self.assertFalse(response.data["duplicate"])
        self.assertEqual(response.data["previous_version"], previd)


    @patch('file.views.DjangoFile')
    @patch('file.views.hash_chunked_file', new=lambda x:'filehash1')
    def test_process_file(self, djangofile_class_mock):
        request = MagicMock()
        request.POST = {'local_path':'', 'local': False}
        filemock = MagicMock()
        filemock.name = 'name'
        filemock.open.return_value = "open1"
        request.FILES = {'file':filemock}
        djangofile_class_mock.return_value = 'djangofile'
        slf = MagicMock()
        slf.process_local_file = MagicMock()
        slf.process_local_file.return_value = 'filehash2'
        with patch("builtins.open", mock_open()) as mock_file:
            ruploaded_file, rfile_hash = FileView.process_file(slf, request)
            self.assertEqual(rfile_hash, 'filehash1')
            self.assertEqual(ruploaded_file, 'djangofile')

            request = MagicMock()
            request.POST = {'local_path':'/local/path', 'local': True}
            request.FILES = {}
            ruploaded_file, rfile_hash = FileView.process_file(slf, request)
            self.assertEqual(rfile_hash, 'filehash2')
            self.assertEqual(ruploaded_file, 'djangofile')

            request = MagicMock()
            request.POST = {}
            request.FILES = {}
            with self.assertRaisesRegex(LocalFileError,"no file specified") as err:
                ruploaded_file, rfile_hash = FileView.process_file(slf, request)


    def test_post(self):
        pass
        # self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        # # TODO test when file_hash is none
        # from file.views import Response

        # slf = MagicMock()
        # request = MagicMock()
        # slf.process_file = MagicMock()
        # slf.process_file.return_value = (None , None)
        # slf.process_file.side_effect = LocalFileError('test1')
        # rval = FileView.post(slf, request)
        # self.assertIsInstance(rval, Response)
        # self.assertEqual(rval.status, 403)

        # slf = MagicMock()
        # slf.process_file = MagicMock()
        # slf.process_file.return_value = (a,b)

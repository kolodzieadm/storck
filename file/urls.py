from django.urls import path

from .views import ListFileView, FileView

urlpatterns = [
    path('files', ListFileView.as_view(), name="all-files"),
    path('file', FileView.as_view(), name="single-file"),
]

import os
from django.db import models
from django.contrib.auth.models import User
from workspace.models import Workspace
from django.conf import settings
from .utils import get_storck_file_path, hash_file, hash_large_multi
from django.db.models import Max
from .depr import update_filename



class StorckFile(models.Model):
    id = models.AutoField(primary_key=True)
    file = models.FileField( default=None)
    hash = models.TextField(default=None, null=True)
    workspace = models.ForeignKey(Workspace, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    previous_version = models.ForeignKey(
        "self",
        on_delete=models.CASCADE,
        related_name="%(class)s_previous_version",
        default=None,
        null=True,
    )
    duplicate_of = models.ForeignKey(
        "self",
        on_delete=models.CASCADE,
        related_name="%(class)s_duplicate_of",
        null=True,
    )
    date = models.DateTimeField(auto_now_add=True)
    fake_path = models.TextField(default=None, null=True)
    meta_open = models.TextField(default=None, null=True)
    meta_closed = models.TextField(default=None, null=True)
    hide = models.BooleanField(default=False)


    def __str__(self):
        return "{}, {}".format(self.date, self.hash)

from django.shortcuts import render, redirect
from django.contrib import auth
from rest_framework.authtoken.models import Token

from .utils import get_current_git_commit


def main(request):
    token = None
    if request.user.is_authenticated:
        token = Token.objects.get(user=request.user)
    context = {'commit_hash': get_current_git_commit(), 'token': token, 'user': request.user}
    return render(request, 'main/index.html', context)


def logout(request):
    auth.logout(request)
    return redirect('/view/main')

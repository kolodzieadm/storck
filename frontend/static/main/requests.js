import { setAuthData, getAuthData, getWorkspaceToken } from "../utils/token.js";

export const uploadFile = async ({ file, meta, name }) => {
    let formData = new FormData();

    if (meta)
        formData.append('meta', meta);
    formData.append('file', file);


    try {
        const response = await fetch(`/api/file?token=${getWorkspaceToken()}${name ? `&path=${name}` : ''}`, {
            method: 'POST',
            headers: {
                Authorization: `Token ${getAuthData().token}`,
            },
            body: formData
        });

        if(response.ok)
            return await response.json();
        throw response;
    } catch (err) {
        console.alert("Something went wrong");
    }
};

export const getFile = async (fileId) => {
    try {
        const { token } = getAuthData();
        const response = await fetch(`/api/file?id=${fileId}&token=${getWorkspaceToken()}`, {
            method: 'GET',
            headers: {
                Authorization: `Token ${token}`,
            },
        });

        if(response.ok)
            return await response.blob();
        throw response;
    } catch (err) {
        console.alert("Something went wrong");
    }
};

export const getFilesList = async () => {
    try {
        const { token } = getAuthData();

        const response = await fetch(`/api/files?token=${getWorkspaceToken()}&hidden=True`, {
            method: 'GET',
            headers: {
                Authorization: `Token ${token}`
            }
        });

        if (response.ok)
            return await response.json();

        throw response;
    } catch (err) {
        console.error(err);
    }
};

export const createWorkspace = async (workspaceName) => {
    let formData = new FormData();
    formData.append('name', workspaceName);

    try {
        const response = await fetch('/api/workspace', {
            method: 'POST',
            headers: {
                Authorization: `Token ${getAuthData().token}`,
            },
            body: formData
        });

        if(response.ok)
            return await response.json();
        throw response;
    } catch (err) {
        console.error("Something went wrong");
    }
};

export const addUserToWorkspace = async (userId, workspaceToken) => {
    let formData = new FormData();
    formData.append('workspace_token', workspaceToken);
    formData.append('user_id', userId);

    try {
        const response = await fetch('/api/workspace/user', {
            method: 'POST',
            headers: {
                Authorization: `Token ${getAuthData().token}`,
            },
            body: formData
        });

        if(response.ok)
            return await response.json();
        throw response;
    } catch (err) {
        console.error("Something went wrong");
    }
};

export const getWorkspaces = async () => {
    try {
        const response = await fetch('/api/workspaces', {
            method: 'GET',
            headers: {
                Authorization: `Token ${getAuthData().token}`,
            },
        });

        if(response.ok)
            return await response.json();
        throw response;
    } catch (err) {
        console.error("Something went wrong");
    }
};

export const authUser = async () => {
    const authData = getAuthData();

    try {
        if(!authData || !authData.token) throw "Cannot find token";

        const response = await fetch(`/api/auth`, {
            method: 'POST',
            headers: {
                Authorization: `Token ${authData.token}`
            }
        });

        return response.json();
    } catch (err) {
        window.location.replace(`/view/logout`);
    }
};

import {
    removeAuthData,
    setWorkspaceToken,
    getWorkspaceToken,
    getAuthData,
    setAuthData,
} from "../utils/token.js";
import {
    createExpandableSection,
    createSection,
} from "../utils/commonComponents.js";
import {
    authUser,
    getFile,
    getFilesList,
    getWorkspaces,
    createWorkspace,
    addUserToWorkspace as addUserToWorkspaceRequest,
} from "./requests.js";

const logoutUser = () => {
    removeAuthData();
    window.location.replace(`/view/logout`);
};

const getFileHandler = async (fileId, downloadName) => {
    const blob = await getFile(fileId);

    const a = document.createElement('a');
    a.href = window.URL.createObjectURL(blob);
    a.download = downloadName;

    document.body.appendChild(a);

    a.click();
    a.remove();
};

const addWorkspace = async () => {
    const workspaceName = document.getElementById('workspace_name').value;

    await createWorkspace(workspaceName);
    await updateWorkspaceList();
};

const composeFileRow = ({ id, date, fake_path, file, meta_open, download = 'Download' }) => {
    const rowElement = document.createElement("li");
    rowElement.classList.add('fileItem');

    const idElement = createSection({ mainValue: 'ID', subValue: id });
    const dateElement = createSection({ mainValue: 'Create date', subValue: date });
    const fakePathElement = createSection({ mainValue: 'Fake path', subValue: fake_path });
    const fileElement = createSection({ mainValue: 'File path', subValue: file });
    const metaElement = createSection({ mainValue: 'Open meta data', subValue: meta_open });
    const downloadElement = document.createElement("div");
    const actionsElement = document.createElement("div");
    actionsElement.classList.add('itemActions');

    downloadElement.innerHTML = download;
    if (download === 'Download') {
        downloadElement.onclick = () => getFileHandler(id, fake_path);
        downloadElement.classList.add('actionButton');
    }
    actionsElement.appendChild(downloadElement);

    rowElement.appendChild(idElement);
    rowElement.appendChild(fakePathElement);
    rowElement.appendChild(dateElement);
    rowElement.appendChild(fileElement);
    rowElement.appendChild(metaElement);
    rowElement.appendChild(actionsElement);

    return rowElement;
};

const createWorkspaceUsersList = ({ users = [] }) => {
    const usersList = document.createElement("ul");

    users.forEach(({ id, username }) => {
        const userListElement = document.createElement("li");
        const userSection = createSection({ mainValue: id, subValue: username });

        userListElement.appendChild(userSection);
        usersList.appendChild(userListElement);
    });

    return usersList;
};

const createWorkspaceRow = ({ id, name, token, users }) => {
    const rowElement = document.createElement("li");
    rowElement.classList.add('workspaceItem');

    const nameElement = createSection({ mainValue: 'Name', subValue: name });
    const tokenElement = createSection({ mainValue: 'Token', subValue: token });
    const usersElement = createExpandableSection({ mainValue: 'Users', subValue: '', content: createWorkspaceUsersList({ users }) });
    const chooseElement = document.createElement("button");
    const addUserElement = document.createElement("button");
    const actionsElement = document.createElement("div");
    const userActionGroupElement = document.createElement("div");
    const userIdElement = document.createElement("input");

    userIdElement.id = `workspaceUserId_${id}`;
    userIdElement.type = "text";
    userIdElement.placeholder = "User ID";

    chooseElement.onclick = () => {
        setWorkspaceToken(token);
        updateFilesList();
    };
    addUserElement.onclick = async () => {
        const userId = document.getElementById(userIdElement.id).value;

        if (userId) {
            await addUserToWorkspaceRequest(userId, token);
            await updateWorkspaceList();
        }
    };

    actionsElement.classList.add('itemActions');
    userActionGroupElement.classList.add('actionGroup');
    chooseElement.classList.add('actionButton');
    addUserElement.classList.add('actionButton');

    chooseElement.innerHTML = 'Choose workspace';
    addUserElement.innerHTML = 'Add user';

    userActionGroupElement.appendChild(userIdElement);
    userActionGroupElement.appendChild(addUserElement);
    actionsElement.appendChild(chooseElement);
    actionsElement.appendChild(userActionGroupElement);
    rowElement.appendChild(nameElement);
    rowElement.appendChild(tokenElement);
    rowElement.appendChild(usersElement);
    rowElement.appendChild(actionsElement);

    return rowElement;
};


const updateFilesList = async () => {
    const filesList = await getFilesList();

    const containerElement = document.getElementById('filesListContent');
    containerElement.innerHTML = null;

    document.getElementById('filesNumber').innerHTML = filesList.files.length || 0;
    filesList.files.forEach((file) => {
        const rowElement = composeFileRow(file);
        containerElement.appendChild(rowElement);
    });
};

const updateWorkspaceList = async () => {
    const containerElement = document.getElementById('workspacesList');
    containerElement.innerHTML = null;

    const workspacesList = (await getWorkspaces()).data.workspaces;
    document.getElementById('workspacesNumber').innerHTML = workspacesList.length || 0;

    workspacesList.forEach(({ id, name, tokens, users }) => {
        containerElement.appendChild(createWorkspaceRow({ id, name, token: tokens[0].token, users }));
    });
};

const updateUserData = async () => {
    const userTokenElement = document.getElementById('userToken');
    const userIdElement = document.getElementById('userId');
    const usernameElement = document.getElementById('username');
    setAuthData({ token: userTokenElement.innerHTML })
    const data = await authUser();
    setAuthData({
        token: data.token,
        email: data.email,
        userId: data.user_id,
        username: data.username,
    });

    const authData = getAuthData() || {};
    userTokenElement.innerHTML = authData.token;
    usernameElement.innerHTML = authData.email;
    userIdElement.innerHTML = authData.userId;
};

const initPage = async () => {
    await updateUserData();
    await updateWorkspaceList();

    if (getWorkspaceToken()) {
        await updateFilesList();
    } else {
         const containerElement = document.getElementById('filesListContent');
         containerElement.innerText = "Choose workspace to see files";
    }
};

initPage();

window.logoutUser = logoutUser;
window.createWorkspace = addWorkspace;

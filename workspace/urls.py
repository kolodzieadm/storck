from django.urls import path

from .views import GetUserWorkspacesView, CreateWorkspaceView, AddUserToWorkspace

urlpatterns = [
    path('workspace', CreateWorkspaceView.as_view(), name='create_workspace'),
    path('workspace/user', AddUserToWorkspace.as_view(), name='add_user_to_workspace'),
    path('workspaces', GetUserWorkspacesView.as_view(), name='get_user_workspace')
]

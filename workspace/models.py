from django.db import models
from django.contrib.auth.models import User


class Workspace(models.Model):
    users = models.ManyToManyField(User)
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class WorkspaceToken(models.Model):
    workspace = models.ForeignKey(Workspace, on_delete=models.CASCADE)
    token = models.CharField(max_length=100)
    name = models.CharField(max_length=100, default="")
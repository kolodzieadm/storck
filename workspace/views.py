from builtins import staticmethod, list
import uuid

from rest_framework import views
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User
from .models import Workspace, WorkspaceToken
from workspace.utils import workspace_to_json


class CreateWorkspaceView(views.APIView):
    @staticmethod
    def post(request):
        post_data = request.POST
        workspace_name = post_data.get("name")

        token = request.headers.get('Authorization').replace('Token ', '')
        user = Token.objects.get(key=token).user

        workspace_token = str(uuid.uuid4())
        workspace_token_name = "Default token"

        workspace = Workspace.objects.create(name=workspace_name)
        WorkspaceToken.objects.create(
            workspace=workspace, name=workspace_token_name, token=workspace_token
        )

        workspace.save()
        workspace.users.add(user)

        workspace_tokens = WorkspaceToken.objects.filter(workspace=workspace).values()
        response = workspace_to_json(workspace, list(workspace_tokens))

        return Response({"data": response})


class GetUserWorkspacesView(views.APIView):
    @staticmethod
    def get(request):
        token = request.headers.get('Authorization').replace('Token ', '')
        user = Token.objects.get(key=token).user

        response = {"id": user.pk, "workspaces": []}

        user_workspaces = Workspace.objects.filter(users=user)

        for workspace in user_workspaces:
            workspace_tokens = WorkspaceToken.objects.filter(
                workspace=workspace
            ).values()

            response["workspaces"].append(
                workspace_to_json(workspace, list(workspace_tokens))
            )

        return Response({"data": response})


class AddUserToWorkspace(views.APIView):
    @staticmethod
    def post(request):
        post_data = request.POST
        workspace_token = post_data.get("workspace_token")
        new_user_id = post_data.get("user_id")

        workspace = WorkspaceToken.objects.get(token=workspace_token).workspace
        new_user = User.objects.get(id=new_user_id)

        workspace.users.add(new_user)
        workspace.save()

        response = workspace_to_json(workspace)
        return Response({"data": response})

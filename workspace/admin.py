from django.contrib import admin
from .models import Workspace, WorkspaceToken

admin.site.register(Workspace)
admin.site.register(WorkspaceToken)
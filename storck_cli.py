#!/usr/bin/python3
import argparse
from scripts.storck_client import StorckClient


parser = argparse.ArgumentParser(description='STORCK CLI')
parser.add_argument('action', choices=['get_workspaces', 'create_workspace', 'add_workspace_user', 'get_files', 'get_file', 'download_file', 'upload_file'],
                    help='Action to run')
parser.add_argument('arguments', action='extend', nargs='*',
                    help='Arguments for action')

parser.add_argument('--host', '-a', dest='api_host', action='store',
                    help='STORCK api host')
parser.add_argument('--user-token', '-u', dest='user_token', action='store',
                    help='STORCK user token')
parser.add_argument('--workspace-token', '-w', dest='workspace_token', action='store',
                    help='STORCK workspace token')


class CliScript:
    def __init__(self):
        args = parser.parse_args()
        print(args)
        self.client = StorckClient(api_host=args.api_host, user_token=args.user_token, workspace_token=args.workspace_token)

    def run(self):
        args = parser.parse_args()
        command = self._get_command()
        command(*args.arguments)

    def _get_command(self):
        args = parser.parse_args()
        return getattr(self.client, args.action)


if __name__ == "__main__":
    CliScript().run()

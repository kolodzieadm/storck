# Storck

### 1. Requirements

- Python 3
- Django
- djangorestframework
- requests
 
### 2. Start development
 
 1. First run script `./quick_setup.sh` to install required packages, migrate database & prepare it for tests
 3. Create database superuser with command `python3 manage.py createsuperuser` to have access to admin panel 
 4. Finally run server `python3 manage.py runserver`
 5. Run `python3 manage.py test` for testing

Server should be available at `localhost:8000`
  
### 3. Available endpoints

- Get authorization token

  | Method | endpoint | params |
  | ------ | -------- | ------ |
  | POST | /api/auth | username - (body) |
  | | | password - (body) |
  
  In response you get auth token which is required in all other requests
  
- Verify authorization token

  | Method | endpoint | params |
  | ------ | -------- | ------ |
  | POST | /api/auth/verify | username - (body) |
  | | | password - (body) |
  
  Require authorization token provided in Authorization header as `Token <auth_token>`

- Upload file 
  
  | Method | endpoint | params |
  | ------ | -------- | ------ |
  | POST | /api/file | filename - file name |
  | | | token - (query) workspace token |

  Require authorization token provided in Authorization header as `Token <auth_token>`  
  
- Download file
  
  | Method | endpoint | params |
  | ------ | -------- | ------ |
  | GET | /api/file | file_id - required, id in database |
  | | | info - (query) not required, if set as True then return file info |
  | | | token - (query) workspace token |

  Require authorization token provided in Authorization header as `Token <auth_token>`

- List all files stored in workspace
  
  | Method | endpoint | params |
  | ------ | -------- | ------ |
  | GET | /api/files | token - (query) workspace token |

  Require authorization token provided in Authorization header as `Token <auth_token>`

- Create workspace 
  
  | Method | endpoint | params |
  | ------ | -------- | ------ |
  | POST | /api/workspace | name - (body) workspace name |

  Require authorization token provided in Authorization header as `Token <auth_token>`
  
- Get user workspaces 
  
  | Method | endpoint | params |
  | ------ | -------- | ------ |
  | POST | /api/workspaces | |

  Require authorization token provided in Authorization header as `Token <auth_token>`
  
  Creates workspace - separate place to store files. 
  Username to login to new workspace is combined from username and workspace name provided in body.
  Password is the same as for normal user account.

### 4. Frontend pages

On listed below urls you can find simple UI which handle login, uploading, listing and downloading files: 

- Login page
    - url: /view/login
    - description: login page
    - dev link: [http://localhost:8000/view/login](http://localhost:8000/view/login)

- Main page
    - url: /view/main 
    - description: main page which enable us uploading, listing and downloading files as well as managing our workspaces
    - dev link: [http://localhost:8000/view/main](http://localhost:8000/view/main)

### 5. Examples

Before running the examples make sure to [set this project up correctly](#2-start-development) and have proper [requirements](#1-requirements).
Then in order of running the examples, you have to run the following script:

```bash
python prepare_example_credentials.py
```

**But be carful, this will modify your database, NEVER USE IT ON ACTUAL DATABASE**

In `examples` directory you can find examples on how to use current available endpoints in your app.
Currently there are prepared examples for:

- get workspaces list - [examples/http/get_workspaces.py](/examples/http/get_workspaces.py)
- create workspace - [examples/http/create_workspace.py](/examples/http/create_workspace.py)
- fetch file - [examples/http/get_file.py](/examples/http/get_file.py)
- get file info - [examples/http/get_file_info.py](/examples/http/get_file_info.py)
- list all files - [examples/http/get_files_list.py](examples/http/get_files_list.py)
- upload file - [/examples/http/upload_file.py](/examples/http/upload_file.py)


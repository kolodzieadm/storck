import requests

# THOSE ARE TEST CREDENTIALS.
# NEVER USE THEM OUTSIDE EXAMPLES!!!
workspace_token = "73f19f87-f741-4f53-ae50-a9272dc87ea7"
user_token = "7c817d0d8f0e2b719a9df798fbdefe75cf5ba4be"


def upload_file():
    filename = "../exampleFile.txt"
    files = {"file": open(filename, "rb")}
    content = requests.post(
        "http://localhost:8000/api/file",
        params={"token": workspace_token},
        data={"path": filename},
        files=files,
        headers={"Authorization": "Token " + user_token},
    )
    if content.status_code == 200:
        print("File uploaded successfully")
    else:
        raise ValueError


if __name__ == "__main__":
    upload_file()

import requests
from get_files_list import get_files_list

# THOSE ARE TEST CREDENTIALS.
# NEVER USE THEM OUTSIDE EXAMPLES!!!
workspace_token = '73f19f87-f741-4f53-ae50-a9272dc87ea7'
user_token = '7c817d0d8f0e2b719a9df798fbdefe75cf5ba4be'

files = get_files_list()
file_id = files[0]["id"]


def get_file():
    content = requests.get("http://localhost:8000/api/file?id={}&token={}&info=True".format(file_id, workspace_token), stream=True, headers={'Authorization': 'Token ' + user_token})

    if content.status_code == 200:
        return content.json()["file"]
    else:
        raise ValueError


if __name__ == "__main__":
    file = get_file()
    print(file)

import requests
import os


class StorckClient:
    def __init__(self, api_host="http://localhost:8000", user_token=None, workspace_token=None):
        self.api_host = os.getenv('STORCK_API_HOST', default=api_host)
        self.user_token = os.getenv('STORCK_USER_TOKEN', default=user_token)
        self.workspace_token = os.getenv('STORCK_WORKSPACE_TOKEN', default=workspace_token)

    def auth_verify(self):
        self._is_authorized()
        return self._post(
            '/api/auth/verify',
            headers={'Authorization': 'Token {}'.format(self.user_token)}
        )

    def set_workspace_token(self, workspace_token):
        self.workspace_token = workspace_token.token
        os.putenv('STORCK_WORKSPACE_TOKEN', self.workspace_token)

    def create_workspace(self, name):
        self._is_authorized()
        content = self._post(
            '/api/workspace',
            data={'name': name},
            headers={'Authorization': 'Token {}'.format(self.user_token)}
        )
        return content['data']

    def get_workspaces(self):
        self._is_authorized()
        content = self._get(
            '/api/workspaces',
            headers={'Authorization': 'Token {}'.format(self.user_token)}
        )
        return content['data']['workspaces']

    def get_files(self):
        self._is_authorized()
        self._is_workspace_set()
        content = self._get(
            '/api/files',
            query={'token': self.workspace_token},
            headers={'Authorization': 'Token {}'.format(self.user_token)}
        )
        return content['files']

    def get_file(self, file_id=None, path=None):
        self._is_authorized()
        self._is_workspace_set()
        content = self._get(
            '/api/file',
            query={'info': True, 'path': path, 'id': file_id, 'token': self.workspace_token},
            headers={'Authorization': 'Token {}'.format(self.user_token)}
        )
        return content['file']

    def upload_file(self, filename, path=None):
        self._is_authorized()
        self._is_workspace_set()
        return self._post(
            '/api/file',
            query={'token': self.workspace_token},
            data={"path": path or filename},
            files={"file": open(filename, "rb")},
            headers={'Authorization': 'Token {}'.format(self.user_token)}
        )

    def download_file(self, file_id):
        self._is_authorized()
        self._is_workspace_set()
        return self._get_raw(
            '/api/file',
            query={'id': file_id, 'token': self.workspace_token},
            headers={'Authorization': 'Token {}'.format(self.user_token)}
        )

    def add_user_to_workspace(self, user_id):
        self._is_authorized()
        self._is_workspace_set()
        content = self._post(
            '/api/workspace/user',
            data={'user_id': user_id, 'token': self.workspace_token},
            headers={'Authorization': 'Token {}'.format(self.user_token)}
        )
        return content['data']

    def _is_authorized(self):
        if self.user_token is None:
            raise Exception('You need to provide user token')

    def _is_workspace_set(self):
        if self.workspace_token is None:
            raise Exception('You need to provide workspace token')

    def _post(self, path, query=None, data=None, files=None, headers=None):
        content = requests.post("{}{}".format(self.api_host, path), data=data, files=files, params=query,
                                headers=headers)
        content.raise_for_status()
        return content.json()

    def _get(self, path, query=None, headers=None):
        content = requests.get("{}{}".format(self.api_host, path), params=query, headers=headers)
        content.raise_for_status()
        return content.json()

    def _get_raw(self, path, query=None, headers=None):
        content = requests.get("{}{}".format(self.api_host, path), params=query, headers=headers, stream=True)
        content.raise_for_status()
        return content.raw.read()

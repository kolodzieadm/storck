#!/bin/bash

mkdir media
mkdir media/uploads
python3 -m pip install -r requirements.txt
python3 manage.py migrate
DJANGO_SUPERUSER_PASSWORD=admin python3 manage.py createsuperuser --no-input --username admin --email admin@bar.com
sqlite3 db.sqlite3 "INSERT INTO file_file (file, date, user_id, workspace_id, hide, meta_open, meta_closed) values ('uploads/exampleFile_1.txt', '2019-07-16 20:21:53.643181', '1', '1', False, '', '');"
sqlite3 db.sqlite3 "INSERT INTO file_file (file, date, user_id, workspace_id, hide, meta_open, meta_closed) values ('uploads/exampleFile_2.txt', '2019-07-16 20:21:53.643181', '1', '1', False, '', '');"
sqlite3 db.sqlite3 "INSERT INTO file_file (file, date, user_id, workspace_id, hide, meta_open, meta_closed) values ('uploads/exampleFile_3.txt', '2019-07-16 20:21:53.643181', '1', '1', False, '', '');"
cp examples/exampleFile.txt media/uploads/exampleFile_1.txt
cp examples/exampleFile.txt media/uploads/exampleFile_2.txt
cp examples/exampleFile.txt media/uploads/exampleFile_3.txt

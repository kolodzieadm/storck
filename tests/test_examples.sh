#!/usr/bin/env bash

cd examples/http
set -e
python prepare_example_credentials.py -y
python upload_file.py
python auth_user.py
python create_workspace.py
python get_file_by_path.py
python get_file_info.py
python get_file.py
python get_files_list.py
python get_workspaces.py
